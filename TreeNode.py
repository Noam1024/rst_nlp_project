class TreeNode:
    def __init__(self, span_start, span_end, text, nuclearity=None, relation_type=None):
        self.span_start = span_start
        self.span_end = span_end
        self.nuclearity = nuclearity
        self.relation_type = relation_type
        self.text = text

        self.parent = None
        self.left_child, self.right_node = None, None
        self.nucleus_edu = None
        self.nucleus_span = None

    def tag(self, nuclearity, relation_type, parent):
        self.nuclearity = nuclearity
        self.relation_type = relation_type
        self.parent = parent

    def is_internal_node(self):
        return self.span_start != self.span_end


class InternalNode(TreeNode):
    def __init__(self, left_child: TreeNode, right_child: TreeNode, left_nuclearity, right_nuclearity, relation_type):
        """
        For bottom up initialization
        """
        text = left_child.text + right_child.text
        span_start = left_child.span_start
        span_end = right_child.span_end
        super().__init__(span_start, span_end, text)

        same_nuclearity = left_nuclearity == right_nuclearity
        left_relation = relation_type if left_nuclearity == 'Satellite' or same_nuclearity else 'SPAN'
        left_child.tag(left_nuclearity, left_relation, self)
        right_relation = relation_type if right_nuclearity == 'Satellite' or same_nuclearity else 'SPAN'
        right_child.tag(right_nuclearity, right_relation, self)

        self.left_child = left_child
        self.right_node = right_child

    def is_internal_node(self):
        return True
