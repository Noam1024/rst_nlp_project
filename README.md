# rst_nlp_project

## Google Colab GPU notebook

I created a jupyter notebook on Google Colab, 
which can be found on 
[this link](https://colab.research.google.com/drive/1JMlQycV6bWcHGldHB5K_N38l3qV4XaHX), 
to run our code on a Google's gpu.

* The first block installs python and its packages. 
* The second one clones our code into the notebook filesystem 
(which you may browse on the files tab on the left). 
* The next blocks actually run it. 
* The last section lets you save the learned parameters to your drive, for re-usage.

## span_prediction_only branch

The results on the notebook were not sufficiently good, 
nowhere close to scores published on the replication study mentioned in the project description. 
Since each score is dependant on the other, I decided I should first try to maximize the span score, 
and only later think about the others.

This score judges the tree structure, 
which is determined only by shift/reduce actions, without labelling nuclearity and relations. 
This makes the labels dimension very small, with only 2 classes, which leads to slightly faster training. 

It seems the span score for similar model arguments on the master branch and on this branch are similar 
with defaulted 100 hidden size. 
This means that not all neurons are required for span prediction, 
but it also made me believe that improvements on this branch will improve the overall score 
when predicting everything together.

## parameters

There are a lot of parameters to the model, 
and I needed an easy way to configure them easily, for the experiments on the notebook, 
without filling the git with garbage commits.
So, I've added cli arguments to run the model. 
You can still change the defaults via the config class.

`usage: DiscourseModel.py [-h] [-t] [-r] [-e]
                         [-d HIDDEN_SIZE [HIDDEN_SIZE ...]] [-l LEARNING_RATE]
                         [-n N_EPOCHS] [-b BATCH_SIZE] [-V VOCABULARY_SIZE]
                         [-s TOKENIZER_SCORE]
`

`-h` is for help, obviously, not really relevant.

`-t`, `-r` and `-e` control what you run. 
Both t and r train, but r restores the saved session if it exists. 
e loads the saved session and writes its dev predictions. 
You should choose at least on of them.

`-d` controls the hidden size, defaults to 100. 
I started experimenting with deeper nets on the span_only branch, 
so you can determine more size by writing `-d 200 150 100`. 
Didn't lead to better results with the span.

`-l` is the learning rate. 
Defaults to 0.001, but I usually used 0.0001, and sometimes 0.00001 when restoring a previously trained model. 
I reduced the LR because the difference in the loss was supposed to be more small.
Usually when restoring, the dev score is smaller, because of over fitting or 
because the optimizer we use change its actual LR over the course of the gradient descent, 
so when rerunning it from epoch 0 again it doesn't optimize correctly.
 
`-n` epochs number, defaults to 100, I used 300.

`-b` batch size, defaults to 50, didn't try to change it.

`-V` determines the vocabulary size of the tokenizer. 
The tokenizer keeps all its words, but when calling `texts_to_matrix`, 
only the most common V-1 words will be tokenized and the other will be ignored. 
Common practice on NLP, to reduce the noise of relatively rare words. 
Defaults to None, so all words are kept. I found out 250 are enough words to get around 64%-66% span score, 
but it might be a local optimum of the current model, and other models will use more or less words.

`-s` determines the tokenizer score of the words. 
Defaults to count, which outputs a bag of words, with count of every tokenized word. 
The other options are "binary", "tfidf", and "freq". 
tfidf is your best option, unless you want to use another word representation like embeddings.

## What's next?

Well, you decide, but here's some suggestions:

### nuclearity-only and relation-only nets

You could go with the direction of the span-only, and predict the nuclearity and relation in another net or more, 
only after "reduce" action was predicted. 
This way this net will only train and predict with the stack inputs, without the queue input as a distraction. 
With one net, you will still have 40 classes to predict from.
 
You could have more nets, by predicting first the nuclearity, and then the relation. 
This way you could have dedicated relation prediction networks for each of {(N, S), (S, N), (N, N)} or 
direct (N, S) and (S, N) to the same network, but always send the nuclear as the first input, 
and the satellite as the second.

### Improving the span_only

Right now, the span only branch predicts two logits for the two classes, 
and picks the larger one. If you take the road presented in the previous section and keep a network to predict span,
it might be better to predict a single number, and choose action if it's smaller or larger than 0.5. 

I tried to do it, but reverted the commit because it didn't work, but it's possible I didn't do it right.

### Add features

Something I have in mind is adding another tokenizer, only for the first word in the stack item's text. 
It could have smaller vocabulary size, so it will not add a lot of dimensionality.

### RNN? embeddings?

Maybe change the whole model structure and the word representations, I don't know

### Refactoring to keras or pytorch

It probably will not improve the scores, but at least the code will look better

### ~~Submit what we have~~

~~We didn't have much free time this summer, and it's considered a hard problem.~~

~~If nothing works and no time is left, at least we tried.~~