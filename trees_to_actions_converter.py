from dataclasses import dataclass
from os import listdir
from os.path import join


class StackItem:
    def __init__(self, span_start, span_end, nuclearity=None, relation=None):
        self.span_start = span_start
        self.span_end = span_end
        self.nuclearity = nuclearity
        self.relation = relation
        self.left = None
        self.right = None

    def get_edus(self, edus):
        return ''.join(edus[self.span_start - 1:self.span_end])


class State:
    def __init__(self, queue_top, stack):
        self.v1, self.v2 = get_stack_top(stack)
        self.v3 = queue_top


@dataclass(order=True, unsafe_hash=True)
class Action:
    action_type: str = 'shift'
    left_nuclearity: str = None
    right_nuclearity: str = None
    left_relation: str = None
    right_relation: str = None


def get_stack_top(stack):
    if len(stack) == 0:
        return None, None
    if len(stack) == 1:
        return stack[0], None
    return stack[-1], stack[-2]


def generate_tree_actions(file_path):
    with open(file_path, "r") as f:
        lines = f.readlines()
    with open(file_path + '.out.edus') as f_edus:
        edus = f_edus.readlines()
        current_queue_top = len(edus)
    actions = []
    stack = []
    for i in range(len(lines) - 1, -1, -1):
        span_start, span_end, nuclearity, relation = lines[i].split()
        if span_start == span_end:
            assert int(span_start) == current_queue_top
            actions.append((State(current_queue_top, stack), Action()))
            stack.append(StackItem(int(span_start), int(span_end), nuclearity, relation))
            current_queue_top -= 1
        else:
            left_child, right_child = get_stack_top(stack)
            actions.append((State(current_queue_top, stack),
                            Action('reduce',
                                   left_child.nuclearity, right_child.nuclearity,
                                   left_child.relation, right_child.relation)))
            stack.pop()
            stack.pop()
            stack.append(StackItem(left_child.span_start, right_child.span_end, nuclearity, relation))
    left_child, right_child = get_stack_top(stack)
    actions.append((State(current_queue_top, stack),
                    Action('reduce',
                           left_child.nuclearity, right_child.nuclearity,
                           left_child.relation, right_child.relation)))
    return actions


def generate_actions(folder_path):
    tree_actions = {}
    files = listdir(folder_path)
    # files = ['1138']
    for file_name in files:
        if file_name.isdigit():
            full_path = join(folder_path, file_name)
            actions = generate_tree_actions(full_path)
            tree_actions[file_name] = actions
    return tree_actions


def convert(trees_path):
    tree_actions = generate_actions(trees_path)


if __name__ == '__main__':
    import sys

    trees_path = sys.argv[1]
    convert(trees_path)
