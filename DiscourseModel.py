import random
from os import listdir
from os.path import join
from pathlib import Path
import argparse

import numpy as np
import tensorflow as tf
from tqdm import tqdm

from trees_to_actions_converter import generate_actions, Action, StackItem

tf1 = tf.compat.v1


class Config:
    hidden_size = [100]
    learning_rate = 0.001
    n_epochs = 100
    batch_size = 50
    vocabulary_size = None
    tokenizer_score = 'count'
    first_words_size = 50
    # TODO add the files paths here


def extract_words(text):
    text_split = text.split()
    return (text_split[0], text_split[-1]) if len(text) > 0 else ('', '')


class DiscourseModel:
    def __init__(self, config, training_directory, trees_actions):
        self.config = config
        self.depth = len(self.config.hidden_size)
        self.input_size = None
        self.output_size = None

        self.relations = []
        self.actions = []
        self.action_to_id = {}

        self.tokenizer = None
        self.first_word_tokenizer = None
        self.last_word_tokenizer = None
        self.input_placeholder = None
        self.labels_placeholder = None
        self.prediction = None
        self.loss = None
        self.train_op = None

        self.build(training_directory, trees_actions)

    def initialize_tokens(self, training_directory):
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=self.config.vocabulary_size)
        self.first_word_tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=self.config.first_words_size)
        self.last_word_tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=self.config.first_words_size)
        files = listdir(training_directory)
        for file_name in files:
            if file_name.endswith('.edus'):
                full_path = join(training_directory, file_name)
                with open(full_path, "r") as f:
                    lines = f.readlines()
                self.tokenizer.fit_on_texts(lines)
                for line in lines:
                    first_word, last_word = extract_words(line)
                    self.first_word_tokenizer.fit_on_texts([first_word])
                    self.last_word_tokenizer.fit_on_texts([last_word])
        vocabulary_size = len(self.tokenizer.index_word) + 1 if self.config.vocabulary_size is None \
            else self.config.vocabulary_size
        self.input_size = (vocabulary_size + self.config.first_words_size*2 + 1) * 3

    def initialize_actions(self, trees_actions):
        self.actions = []
        self.action_to_id = {}
        actions_set = set()
        for tree_name, state_actions in trees_actions.items():
            for _, action in state_actions:
                actions_set.add(action)
        for i, action in enumerate(sorted(list(actions_set))):
            self.actions.append(action)
            self.action_to_id[action] = i
        self.output_size = len(self.actions)

    def add_placeholders(self):
        """Generates placeholder variables to represent the input tensors

        These placeholders are used as inputs by the rest of the model building and will be fed
        data during training.  Note that when "None" is in a placeholder's shape, it's flexible
        (so we can use different batch sizes without rebuilding the model).

        Adds following nodes to the computational graph

        input_placeholder: Input placeholder tensor of  shape (None, vocabulary), type tf.int32
        labels_placeholder: Labels placeholder tensor of shape (None, len(self.actions)), type tf.int32
        # mask_placeholder:  Mask placeholder tensor of shape (None, self.max_length), type tf.bool
        # dropout_placeholder: Dropout value placeholder (scalar), type tf.float32
        """
        # Assuming the input is 3 bag-of-words
        self.input_placeholder = tf1.placeholder(dtype=tf.float32,
                                                 shape=(None, self.input_size),
                                                 name='input')

        self.labels_placeholder = tf1.placeholder(dtype=tf.int32,
                                                  shape=(None,),
                                                  name='labels')

    def add_prediction_op(self):
        dimensions = [self.input_size] + self.config.hidden_size + [self.output_size]
        depth = len(dimensions) - 1
        activations = [tf.nn.sigmoid] * depth  # might be different fot each layer?
        previous_layer = self.input_placeholder
        for i in range(depth):
            w = tf1.get_variable(dtype=tf.float32,
                                 shape=(dimensions[i], dimensions[i+1]),
                                 name=f'w_{i}',
                                 initializer=tf.contrib.layers.xavier_initializer())
            b = tf1.get_variable(dtype=tf.float32, shape=(dimensions[i+1],), name=f'b_{i}')

            layer_inner = tf.add(tf.matmul(previous_layer, w), b)
            if i < depth - 1:
                previous_layer = activations[i](layer_inner)
        return layer_inner

        '''
        A = tf1.get_variable(dtype=tf.float32,
                             shape=(self.input_size, self.config.hidden_size),
                             name='A',
                             initializer=tf.contrib.layers.xavier_initializer())
        b1 = tf1.get_variable(dtype=tf.float32, shape=(self.config.hidden_size,), name='b1')

        hidden_inner = tf.add(tf.matmul(self.input_placeholder, A), b1)
        hidden = tf.nn.sigmoid(hidden_inner)

        w = tf1.get_variable(dtype=tf.float32,
                             shape=(self.config.hidden_size, self.output_size),
                             name='w',
                             initializer=tf.contrib.layers.xavier_initializer())
        b2 = tf1.get_variable(dtype=tf.float32, shape=(self.output_size,), name='b2')
        prediction = tf.add(tf.matmul(hidden, w), b2)

        return prediction
        '''

    def add_loss_op(self, prediction):
        batch_loss = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.labels_placeholder,
                                                                    logits=prediction)
        loss = tf.reduce_mean(batch_loss)
        return loss

    def add_training_op(self, loss):
        optimizer = tf1.train.AdamOptimizer(learning_rate=self.config.learning_rate)
        train_op = optimizer.minimize(loss=loss)
        return train_op

    # def create_feed_dict(self, inputs_batch, mask_batch, labels_batch=None, dropout=1.0):
    def create_feed_dict(self, inputs_batch, labels_batch=None):
        """Creates the feed_dict for the dependency parser.

        A feed_dict takes the form of:

        feed_dict = {
                <placeholder>: <tensor of values to be passed for placeholder>,
                ....
        }

        Args:
            inputs_batch: A batch of input data.
            # mask_batch:   A batch of mask data.
            labels_batch: A batch of label data.
            # dropout: The dropout rate.
        Returns:
            feed_dict: The feed dictionary mapping from placeholders to values.
        """
        feed_dict = {}

        if inputs_batch is not None:
            feed_dict[self.input_placeholder] = inputs_batch

        if labels_batch is not None:
            feed_dict[self.labels_placeholder] = labels_batch

        # if mask_batch is not None:
        #     feed_dict[self.mask_placeholder] = mask_batch
        #
        # if dropout is not None:
        #     feed_dict[self.dropout_placeholder] = dropout

        return feed_dict

    def train(self, session, training_directory, trees_actions, saver, *, should_restore=False):
        model_path = str(Path('saved') / 'model.weights')
        if should_restore:
            saver.restore(session, model_path)

        total_actions = 0
        edus_by_tree_name = {}
        for tree_name, state_actions in trees_actions.items():
            total_actions += len(state_actions)
            with open(join(training_directory, tree_name + '.out.edus')) as tree_file:
                edus_by_tree_name[tree_name] = tree_file.readlines()

        batch_amount = total_actions // self.config.batch_size

        best_epoch = float('inf')
        for epoch in range(self.config.n_epochs):
            epoch_best = float('inf')
            epoch_avg = 0.
            for i in tqdm(range(batch_amount)):
                inputs_batch = np.zeros((self.config.batch_size, self.input_size))
                labels_batch = []
                for j in range(self.config.batch_size):
                    tree_name, state_actions = random.choice(list(trees_actions.items()))
                    edus = edus_by_tree_name[tree_name]
                    state, action = random.choice(state_actions)
                    v1 = state.v1.get_edus(edus) if state.v1 is not None else ''
                    v2 = state.v2.get_edus(edus) if state.v2 is not None else ''
                    v3 = edus[state.v3 - 1]

                    bag_of_words = self.tokenizer.texts_to_matrix([v1, v2, v3], self.config.tokenizer_score)
                    first_words, last_words = zip(extract_words(v1),
                                                  extract_words(v2),
                                                  extract_words(v3))
                    first_words_one_hot = self.first_word_tokenizer.texts_to_matrix(first_words, 'binary')
                    last_words_one_hot = self.last_word_tokenizer.texts_to_matrix(last_words, 'binary')
                    stacked_inputs = np.hstack([bag_of_words, first_words_one_hot, last_words_one_hot,[[len(v1)],[len(v2)],[len(v3)]]])
                    inputs_batch[j] = stacked_inputs.reshape((1, -1))

                    label = self.action_to_id[action]
                    labels_batch.append(label)

                feed = self.create_feed_dict(inputs_batch, labels_batch=labels_batch)
                _, loss = session.run([self.train_op, self.loss], feed_dict=feed)

                if loss < epoch_best:
                    epoch_best = loss
                epoch_avg += loss / float(batch_amount)
                # loss = session.run([self.loss], feed_dict=feed)

            print(f'epoch {epoch} - loss: {loss}, best: {epoch_best}, avg: {epoch_avg}')
            if epoch_avg < best_epoch:
                saver.save(session, model_path)

    def build(self, training_directory, trees_actions):
        self.initialize_tokens(training_directory)
        self.initialize_actions(trees_actions)
        self.add_placeholders()
        self.prediction = self.add_prediction_op()
        self.loss = self.add_loss_op(self.prediction)
        self.train_op = self.add_training_op(self.loss)

    def predict(self, session, dev_directory):
        files = listdir(dev_directory)
        for file_name in files:
            if file_name.endswith('.edus'):
                with open(dev_directory / file_name, 'r') as file:
                    edus = file.readlines()

                queue_top = len(edus) - 1
                stack = []

                while queue_top >= 0 or len(stack) > 1:
                    action = Action()

                    if len(stack) > 1:
                        v1 = stack[-1].get_edus(edus)
                        v2 = stack[-2].get_edus(edus)
                        v3 = edus[queue_top] if queue_top >= 0 else ''

                        bag_of_words = self.tokenizer.texts_to_matrix([v1, v2, v3], self.config.tokenizer_score)
                        first_words, last_words = zip(extract_words(v1),
                                                      extract_words(v2),
                                                      extract_words(v3))
                        first_words_one_hot = self.first_word_tokenizer.texts_to_matrix(first_words, 'binary')
                        last_words_one_hot = self.last_word_tokenizer.texts_to_matrix(last_words, 'binary')
                        stacked_inputs = np.hstack([bag_of_words, first_words_one_hot, last_words_one_hot, [[len(v1)],[len(v2)],[len(v3)]]])
                        inputs = stacked_inputs.reshape((1, -1))
                        feed = self.create_feed_dict(inputs)

                        logits = session.run([self.prediction], feed)

                        action = self.actions[np.argmax(logits)]
                        next_best_action = self.actions[np.argsort(logits)[0, 0, -2]]

                        if action.action_type == 'shift' and queue_top < 0:
                            action = next_best_action

                    if action.action_type == 'shift':
                        if queue_top < 0:
                            raise Exception('Trying to shift an empty queue')
                        stack_item = StackItem(queue_top + 1, queue_top + 1)
                        stack.append(stack_item)
                        queue_top -= 1
                    else:  # reduce
                        top_stack = stack.pop()
                        top_stack.nuclearity = action.left_nuclearity
                        top_stack.relation = action.left_relation
                        next_in_stack = stack.pop()
                        next_in_stack.nuclearity = action.right_nuclearity
                        next_in_stack.relation = action.right_relation
                        stack_item = StackItem(top_stack.span_start, next_in_stack.span_end)
                        stack_item.left = top_stack
                        stack_item.right = next_in_stack
                        stack.append(stack_item)

                root = stack[0]
                # print_node(root)

                with open(dev_directory / 'predictions' / file_name.replace('.out.edus', ''), 'w') as file:
                    write_predictions(root, file)
                # print(stack)


def write_predictions(node, file):
    nuclearity = node.nuclearity if node.nuclearity is not None else 'Root'
    rel = node.relation if node.relation is not None else ''
    start = node.span_start
    end = node.span_end

    if nuclearity != 'Root':
        file.write(f'{start} {end} {nuclearity} {rel}\n')

    if node.left is not None:
        write_predictions(node.left, file)

    if node.right is not None:
        write_predictions(node.right, file)


def print_node(node, indent=''):
    nuclearity = node.nuclearity if node.nuclearity is not None else 'Root'
    rel = node.relation if node.relation is not None else ''
    start = node.span_start
    end = node.span_end
    # print(f'{indent}( {nuclearity} (span {start} {end}) {rel}')
    if node.left is not None:
        print_node(node.left, indent + '  ')

    if node.right is not None:
        print_node(node.right, indent + '  ')

    # print(f'{indent})')


def train(training_directory, config, *, should_restore=False):
    trees_actions = generate_actions(training_directory)
    with tf.Graph().as_default():
        model = DiscourseModel(config, training_directory, trees_actions)
        saver = tf1.train.Saver()
        init = tf1.global_variables_initializer()
        with tf1.Session() as session:
            session.run(init)
            model.train(session, training_directory, trees_actions, saver, should_restore=should_restore)


def dev(training_directory, dev_directory, config):
    trees_actions = generate_actions(training_directory)
    with tf.Graph().as_default():
        model = DiscourseModel(config, training_directory, trees_actions)
        saver = tf1.train.Saver()
        init = tf1.global_variables_initializer()
        with tf1.Session() as session:
            session.run(init)
            model_path = str(Path('saved') / 'model.weights')
            saver.restore(session, model_path)
            model.predict(session, dev_directory)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Trains or tests a discourse model (choose at least one of them).')

    parser.add_argument('-t', '--train', action='store_true', dest='should_train',
                        help='Trains the model and saves the best parameters.')
    parser.add_argument('-r', '--restore', action='store_true', dest='should_restore',
                        help='Resumes the model training from previously saved parameters.')
    parser.add_argument('-e', '--evaluate', action='store_true', dest='should_evaluate_dev',
                        help='Evaluates the model against the dev set ground truth')

    parser.add_argument('-d', '--hidden-size', type=int, dest='hidden_size', default=Config.hidden_size, nargs='+')
    parser.add_argument('-l', '--learning-rate', type=float, dest='learning_rate', default=Config.learning_rate)
    parser.add_argument('-n', '--n-epochs', type=int, dest='n_epochs', default=Config.n_epochs)
    parser.add_argument('-b', '--batch-size', type=int, dest='batch_size', default=Config.batch_size)

    parser.add_argument('-V', '--vocabulary-size', type=int, dest='vocabulary_size', default=Config.vocabulary_size,
                        help='Only the most common V-1 words will be kept by the tokenizer.')
    parser.add_argument('-f', '--first-words-size', type=int, dest='first_words_size', default=Config.first_words_size,
                        help='Only the most common f-1 words will be kept by the tokenizer.')
    parser.add_argument('-s', '--tokenizer-score', type=str, dest='tokenizer_score', default=Config.tokenizer_score,
                        help='Each word will be assigned a score. '
                             'One of "binary", "count", "tfidf", "freq". '
                             'Default is to count.')

    args = parser.parse_args()
    data_path = Path('data')

    if not (args.should_train or args.should_restore or args.should_evaluate_dev):
        parser.parse_args(['--help'])

    if args.should_train or args.should_restore:
        train(data_path / 'TRAINING', args, should_restore=args.should_restore)

    if args.should_evaluate_dev:
        dev(data_path / 'TRAINING', data_path / 'DEV', args)
