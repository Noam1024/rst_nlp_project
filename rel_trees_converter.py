from os import listdir
from os.path import join

from TreeNode import TreeNode, InternalNode
from utils import map_to_cluster


def get_node(line, line_index, tree_structure):
    if line[0][0] == ')':
        return None, line_index
    nuclearity = line[1]
    is_root = nuclearity == 'Root'
    is_leaf = line[2] == '(leaf'
    if is_leaf:
        span_start = int(line[3][:-1])
        span_end_index = 3
    else:
        span_start = int(line[3])
        span_end_index = 4
    span_end = int(line[span_end_index][:-1])
    if not is_root:
        relation_type = line[span_end_index + 2][:-1]
        clustered_relation_type = map_to_cluster(relation_type)
        edu_text = line[span_end_index + 4:]
        tree_node = TreeNode(span_start, span_end, edu_text, nuclearity, clustered_relation_type)
        tree_structure.append(tree_node)
    else:
        tree_node = TreeNode(span_start, span_end, '', nuclearity)
    return tree_node, line_index


def parse_tree_line(lines, line_index, tree_structure):
    # print(line_index)
    if line_index >= len(lines):
        return None, line_index
    line = lines[line_index].split()
    tree_node, line_index = get_node(line, line_index, tree_structure)
    is_internal_node = tree_node is not None and tree_node.is_internal_node()
    if is_internal_node:
        children = []
        while True:
            child, line_index = parse_tree_line(lines, line_index + 1, tree_structure)
            if child is not None:
                children.append(child)
            else:
                break
        if len(children) > 2:
            new_parent = children[-1]
            for i in range(len(children)-2, 0, -1):
                left_nuclearity = children[i].nuclearity
                right_nuclearity = children[i+1].nuclearity
                relation_type = children[i+1].relation_type
                new_parent = InternalNode(children[i], new_parent, left_nuclearity,
                                          right_nuclearity, relation_type)
                tree_structure.append(new_parent)
            new_parent.tag(children[1].nuclearity, children[1].relation_type, tree_node)
    return tree_node, line_index


def load_tree(file_path):
    tree_structure = []
    with open(file_path, "r") as f:
        lines = f.readlines()
    parse_tree_line(lines, 0, tree_structure)
    tree_structure.sort(key=lambda node: (node.span_start, -node.span_end))
    return tree_structure


def load_trees(folder_path):
    trees = {}
    files = listdir(folder_path)
    for file_name in files:
        if file_name.endswith('.dis'):
            full_path = join(folder_path, file_name)
            tree = load_tree(full_path)
            dot_index = file_name.find('.')
            tree_key = file_name[:dot_index]
            trees[tree_key] = tree
    return trees


def convert(out_dis_trees_path):
    out_dis_trees = load_trees(out_dis_trees_path)
    for key, tree in out_dis_trees.items():
        file_name = join(out_dis_trees_path, key)
        with open(file_name, 'w') as file:
            for node in tree:
                file.write(f'{node.span_start} {node.span_end} {node.nuclearity[0]} {node.relation_type}\n')


if __name__ == '__main__':
    import sys
    out_dis_trees_path = sys.argv[1]
    convert(out_dis_trees_path)
